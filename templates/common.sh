#!/bin/bash

# common scripts and environment variables
# USAGE: source common.sh -OR- . common.sh

COMMON_SCRIPTS="common.sh"

J_PID="$$"
J_HOST="$HOSTNAME"
J_HOSTIP="$HOSTIP"

J_PARENT_IP="{{.ParentIP}}"
J_PARENT_PORT="{{.ParentPort}}"
J_PARENT_PROTO="{{.ParentProto}}"
J_RESOURCE_URI="{{.ResourceURI}}"