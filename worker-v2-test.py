# worker-v2-test.py

import jsonrpclib
import json

class Job:
    def __init__(self):
        self.Env = []
        self.Owner = ""
        self.Exec = ""
        self.Workdir = ""
        self.JobID = ""

        self.Stdout = ""
        self.Stderr = ""
        self.Stdin = ""

        self.StreamType = ""

        self.Status = "invalid"
        self.ExitCode = None

        self.PID = None

        self._RawStreams = {}


rpc = jsonrpclib.Server('http://localhost:4900')

meta = {
	
	"env": [{"HELLO": "WOR:D"}],
	"owner": "Chris Pergrossi",
	"exec": "python test_prog.py",
	"workdir": "/home/chris/devroot/python/w.job-hpc",
	"jobid": None,
	"stream_method":	"block"
}

print rpc.runJob( json.dumps(meta) )