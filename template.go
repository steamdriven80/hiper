package main

import (
	"bufio"
	"encoding/json"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"path"

	"html/template"
)

func main() {

	// find our template root

	var TEMPLATE_ROOT string

	TEMPLATE_ROOT = "/home/chris/dev/git/hiper/templates"

	var templateName = os.Args[1]

	///////////////////////////////////////
	// grab our command line

	var args interface{}

	BufIn := bufio.NewReader(os.Stdin)

	InData, _ := BufIn.ReadString(0xA)

	err := json.Unmarshal([]byte(InData), &args)
	if err != nil {
		log.Println("Invalid command line. Please check your syntax and try again.  Valid JSON only!")
		os.Exit(1)
	}

	// templ, ok := input["template"].(string)
	// if ok != true {
	// 	log.Println("Invalid template name, must have \"template\" key of type string")
	// 	os.Exit(1)
	// }

	// arguments, ok := inputs["data"].([]interface{})
	// if ok != true {
	// 	log.Println("Invalid data name, must have \"data\" key of type array")
	// 	os.Exit(1)
	// }

	// data := arguments.(map[string]string)

	// read the template

	var defaults interface{}

	def, err := ioutil.ReadFile(path.Join(TEMPLATE_ROOT, "../defaults.yml"))

	err = yaml.Unmarshal([]byte(def), &defaults)
	if err != nil {
		panic(err)
	}

	fd, err := ioutil.ReadFile(path.Join(TEMPLATE_ROOT, templateName))
	if err != nil {
		log.Println("Invalid template, unable to load from disk. Path: ", path.Join(TEMPLATE_ROOT, templateName))
		os.Exit(1)
	}

	var parsed map[string]interface{}

	parsed = make(map[string]interface{})

	for v, k := range defaults.(map[interface{}]interface{})["default"].(map[interface{}]interface{}) {
		parsed[v.(string)] = k
	}

	for v, k := range defaults.(map[interface{}]interface{})[templateName].(map[interface{}]interface{}) {
		parsed[v.(string)] = k
	}

	// parse and print to STDOUT

	obj := template.New(templateName)
	if err != nil {
		panic(err)
	}

	obj, err = obj.Parse(string(fd))
	if err != nil {
		panic(err)
	}

	for key, val := range args.(map[string]interface{}) {
		parsed[key] = val
	}

	err = obj.Execute(os.Stdout, parsed)
	if err != nil {
		panic(err)
	}
}
