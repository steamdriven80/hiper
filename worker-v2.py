from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer

import logging, os, subprocess, uuid, json

import signal

API = []


def exported(f):
    global API

    API.append({f.__name__: f})

    def LogAllRequests(*args, **kwargs):
        with open("debug.log", "at") as log:
            log.write(
                "[API]: %s (%s) was called @ %s" % (f.__name__, str(args) + "|" + str(kwargs), str(datetime.now())))

        return f(*args, **kwargs)

    LogAllRequests.__name__ = f.__name__

    return LogAllRequests


@exported
def ping(reflect):
    return json.dumps(reflect)


Jobs = []


class Job:
    def __init__(self):
        self.Env = []
        self.Owner = ""
        self.Exec = ""
        self.Workdir = ""
        self.JobID = ""

        self.Stdout = ""
        self.Stderr = ""
        self.Stdin = ""

        self.StreamType = ""

        self.Status = "invalid"
        self.ExitCode = None

        self.PID = None

        self._RawStreams = {}


def verbose(func, desc):
    logging.debug("[%s] = %s" % (str(desc), str(func)))

    return func


@exported
def runJob(metaj):

    meta = json.loads(metaj)

    task = Job()

    #if not 'owner' in meta or not 'exec' in meta or not 'workdir' in meta:
        #return json.dumps({"status": 400, "error": "invalid request parameters, review your request and try again."})

    if not 'jobid' in meta or meta['jobid'] == None:
        meta['jobid'] = str(uuid.uuid4())[0:18]

        verbose(meta['jobid'], "created a unique job ID")

        task.JobID = meta.get("jobid")
        task.Env = meta.get("env")
        task.Exec = meta.get("exec")
        task.Workdir = meta.get("workdir")
        task.Owner = meta.get("owner")

        task._RawStreams["stdout"] = subprocess.PIPE
        task._RawStreams["stderr"] = subprocess.PIPE
        task._RawStreams["stdin"] = None


    verbose(os.chdir(meta["workdir"]), "changing directory to %s" % meta['workdir'])


    # split command to pieces
    bits = meta["exec"].split(" ")

    verbose(bits, "split command into chunks")

    p = subprocess.Popen(bits, stdout=task._RawStreams["stdout"], stderr=task._RawStreams["stdout"])

    task.PID = p.pid

    Jobs.append(task)


    #def timeout(signum, frame):
    #    logging.warn("[%s]: function timeout has elapsed. killing process." % meta["jobid"])

    #    raise Exception("timeout")

    # assigns a timeout function
    #signal.signal(signal.SIGALRM, timeout)

    #signal.alarm(10)


    # run the function (blocking) and stream the output
    try:
        while p.poll():

            for line in task._RawStreams["stdout"]:
                task.Stdout += line

            # check if theres any input to send

            try:

                for line in sys.stdin:
                    task._RawStreams["stdin"].write(line)

                    task.Stdin += line

            except EOFError:

                # DEBUG:

                break

            # pass

            time.sleep(2)

    except Exception, e:

    	print e

        p.terminate()

    task.ExitCode = p.returncode


    result = {}

    for k in task:

    	result[k] = str(task[k])

    	print '%s: %s' % (k, task[k])

    return result


def LoadAndServe(host, port):
    server = SimpleJSONRPCServer((host, port))

    for func in API:
        server.register_function(func.values()[0], func.keys()[0])

    server.serve_forever()


if __name__ == '__main__':
    LoadAndServe('localhost', 4900)
    